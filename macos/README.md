This build is not included since the original repo of this simulator already offers a macos build. Visit [modbus-simlator/osx-binary](https://github.com/riptideio/modbus-simulator/tree/osx-binary).

If the above link is no longer working, contact [rodolfocastillomateluna@gmail.com](mailto:rodolfocastillomatleuna)

**Update**

Run the following to download the macos binary

```bash
./download.sh
```

